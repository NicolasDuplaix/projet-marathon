@extends('layouts.app')

@section('content')
    {{-- Affiche les erreurs --}}
    @if ($errors->any())
        <div class="alert alert-danger"  style="margin-top: 2rem">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{--
        formulaire de saisie d'une histoire
        la fonction 'route' utilise un nom de route
        'csrf_field' ajoute un champ caché qui permet de vérifier
        que le formulaire vient du serveur.
    --}}

    <div class="conteneur-crea-hist">
       <div class="carnetimg">
           <div class="center-crea-hist">
               <form action="{{route('enregistrer_histoire')}}" method="POST" enctype="multipart/form-data">
               {!! csrf_field() !!} 
               <section>
                       
                       <div class="text-center" style="margin-top: 2rem">
                           <h3><i class="far fa-edit"></i> Création d'une histoire</h3>
                       </div>
                       <div class="form-group row">
                           <!-- {{-- le titre  --}} -->
                           <label id="label-titre" class="form-control-label" for="titre">Titre : </label>
                           <div id="input-titre">
                               <!--{{-- input du titre --}} -->
                               <input type="text" class="form-control" name="titre" id="titre" value="{{ old('titre') }}"
                                   placeholder="Titre de l'histoire">
                           </div> <br />
                           <label class="form-control-label" for="pich">Résumé : </label>
                           <div>
                               <!--{{-- input du pich --}} -->
                               <textarea name="pich" id="pich" cols="30" rows="10" placeholder="Résumé de l'histoire"
                                   class="form-control" style="resize: none"></textarea>
                           </div><br />
                           <label id="label-photo" class="form-control-label" for="photo">Image : </label>
                           <div id="input-photo">
                               <!--{{-- input de la photo --}} -->
                               <input type="file" class="form-control" name="photo" id="photo" 
                                   value="Photo de l'histoire">
                           </div> <br /> <br />
                           <label id="label-genre" class="form-control-label" for="genre">Genre : </label>
                           <div id="select-genre">
                               <!--{{-- input du genre --}} -->
                               <select class="custom-select" id="inputGroupSelect01" name="genre">
                                   <option selected> Genre ➧</option>
                                   @foreach ($genres as $genre) 
                                   <option value="{{$genre->id}}">{{ $genre->label }}</option>
                                   @endforeach
                               </select>
                           </div>
                    </section>   
                      
                     <br>
                           <div class="valider">
                               <input id="input-submit" type="submit" value="Valider">
                           </div>
                       </div>
               </form>
           </div>
       </div>
   </div>

@endsection