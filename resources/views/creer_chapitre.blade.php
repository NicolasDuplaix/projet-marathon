@extends('layouts.app')

@section('content')
    {{-- Affiche les erreurs --}}
    @if ($errors->any())
        <div class="alert alert-danger"  style="margin-top: 2rem">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{--
        formulaire de saisie d'un chapitre
        la fonction 'route' utilise un nom de route
        'csrf_field' ajoute un champ caché qui permet de vérifier
        que le formulaire vient du serveur.
    --}}

    <div class="conteneur-crea-hist">
        <div class="carnetimg">
            <div class="center-crea-hist">
                <form action="{{route('enregistrer_chapitre')}}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="text-center" style="margin-top: 2rem">
                        <h3><i class="far fa-edit"></i> Création d'un chapitre</h3>
                    </div>
                    <div class="form-group row">
                    <!-- {{-- le titre  --}} -->
                        <label id="label-titre" class="form-control-label" for="titre">Titre : </label>
                        <div id="input-titre">
                        <!--{{-- input du titre --}} -->
                            <input type="text" class="form-control" name="titre" id="titre" value="{{ old('titre') }}"
                                   placeholder="Titre du chapitre">
                        </div> <br />
                        <label id="label-titrecourt" class="form-control-label" for="titrecourt">Sous-titre : </label>
                        <div id="input-titrecourt">
                        <!--{{-- input du titrecourt --}} -->
                            <input type="text" class="form-control" name="titrecourt" id="titrecourt" value="{{ old('titrecourt') }}"
                                   placeholder="Sous-titre du chapitre">
                        </div> <br />
                        <label class="form-control-label" for="texte">Contenu : </label>
                        <div>
                        <!--{{-- input du texte --}} -->
                            <textarea name="texte" id="texte" cols="30" rows="10"
                                      placeholder="Contenu du chapitre" class="form-control" style="resize: none">{{old('texte')}}</textarea>
                        </div><br />
                        <label class="form-control-label" for="question">Question : </label>
                        <div>
                        <!--{{-- input de la question --}} -->
                            <textarea name="question" id="question" cols="30" rows="3"
                                      placeholder="Contenu de la question" class="form-control" style="resize: none">{{old('question')}}</textarea>
                        </div><br />
                        <label id="label-photo" class="form-control-label" for="photo">Image : </label>
                        <div id="input-photo">
                        <!--{{-- input de la photo --}} -->
                            <input type="file" class="form-control" name="photo" id="photo" value="{{ old('photo') }}" value="Photo de l'histoire">
                        </div> <br /> <br />
                        <label id="label-histoire" class="form-control-label" for="histoire">Histoire : </label>
                        <div id="select-histoire">
                        <!--{{-- input de l'histoire --}} -->
                            <select  class="custom-select" name="histoire" id="inputGroupSelect01">
                                <option selected> Histoire ➧</option>
                                @foreach ($histoires as $histoire)
                                    <option value="{{$histoire->id}}">{{ $histoire->titre }}</option>
                                @endforeach
                            </select>
                        </div><br />
                        <label id="label-premier" class="form-control-label" for="premier">Premier : </label>
                        <div id="select-premier">
                        <!--{{-- input de l'histoire --}} -->
                            <select  class="custom-select" name="premier" id="inputGroupSelect01">
                                <option selected value="0"> 0 ➧</option>
                                <option value="1">1</option>
                            </select>
                        </div>

                    </div>
                    <button type="submit" id="input-submit">Enregistrer</button>
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                </form>
            </div>
        </div>
    </div>

@endsection