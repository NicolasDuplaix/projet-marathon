@extends('layouts.app')

@section('content')
<div class="conteneur-crea-hist">
        <div class="carnetimg">
            <div class="center-crea-hist">
                <form action="{{ route('enregistrer_choix') }}" method="POST">
                    {!! csrf_field() !!}
                    <div class="text-center" style="margin-top: 2rem">
                        <h3><i class="far fa-edit"></i> Lier les chapitres</h3>
                    </div>
                    <div class="form-group row">
                    <label id="label-genre" class="form-control-label" for="histoire">Histoire : </label>
                        <div id="select-genre">
                            <select  class="custom-select" name="histoire" id="inputGroupSelect01">
                                <option selected> histoire ➧</option>
                                @foreach ($histoires as $histoire)
                                <option value="{{$histoire->id}}">{{ $histoire->titre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" id="input-submit">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
@endsection