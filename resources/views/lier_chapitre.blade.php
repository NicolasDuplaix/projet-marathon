@extends('layouts.app')

@section('content')

<div class="conteneur-crea-hist">
        <div class="carnetimg">
            <div class="center-crea-hist">
                <form action="{{route('enregistrer_liaison')}}" method="POST">
                    {!! csrf_field() !!}
                    <div class="text-center" style="margin-top: 2rem">
                        <h3><i class="far fa-edit"></i> Lier les chapitres</h3>
                    </div>
                    <div class="form-group row">
                    <label id="label-genre" class="form-control-label" for="source">Chapitre source : </label>
                        <div id="select-genre">
                            <!--{{-- input du genre --}} -->
                            @php($histoireSelect=null)
                            <select  class="custom-select" name="source" id="inputGroupSelect01">
                                <option selected> chapitres ➧</option>
                                @foreach ($chapitres as $chapitre)
                                <option value="{{$chapitre->id}}">{{ $chapitre->titrecourt }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br />
                        <label id="label-genre" class="form-control-label" for="dest">Chapitre destination : </label>
                        <div id="select-genre">
                            <select  class="custom-select" name="dest" id="inputGroupSelect01">
                                <option selected> chapitres ➧</option>
                                @foreach ($chapitres as $chapitre)
                                <option value="{{$chapitre->id}}">{{ $chapitre->titrecourt }}</option>
                                @endforeach
                            </select>
                        </div><br />
                        <label id="label-titre" class="form-control-label" for="reponse">Réponse : </label>
                        <div id="input-titre">
                            <input type="text" class="form-control" name="reponse" id="reponse" value="{{ old('reponse') }}"
                                   placeholder="Réponse à la question">
                        </div> <br />
                    </div>
                    <button type="submit" id="input-submit">Enregistrer</button>
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                </form>
            </div>
        </div>
    </div>

@endsection
