use App\Chapitre;
@extends('layouts.app')

@section('content')

<div id="affichage-histoire">

        <h1>{{ $chapitre->histoire->titre }}</h1>

        <h2>{{$chapitre->titre}}</h2> <br />
        <h3>{{ $chapitre->titrecourt }}</h3>
        <div class="affichage-histoire-image" style="background-image: url({{$chapitre->photo}})"></div>
        <p>{{$chapitre->texte}}</p><br />
    
        <p>{{ $chapitre->question }}</p><br />
        <div>
            @foreach($chapitre->suites as $suite)
                <a href="{{ route('store_avancement' ,['id'=>$suite->pivot]) }}">{{$suite->pivot->reponse}}</a>
            @endforeach
        </div>
        <br /><br /><br /><br />
        <div>
             
        </div>
    </div>


@endsection