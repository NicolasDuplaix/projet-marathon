@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css/style_login.css') }}">
@endsection

@section('content')
<div class="conteneur-crea-hist">
        <div class="carnetimg">
            <div class="center-crea-hist">
                <form action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="text-center" style="margin-top: 2rem">
                        <h3><i class="far fa-edit"></i>Authentification</h3>
                    </div>
                    <div class="form-group row">
                        <!-- {{-- le login  --}} -->
                        <label id="label-login" class="form-control-label" for="email">Identifiant </label>
                        <div id="login">
                            <!--{{-- input du login --}} -->
                            <input placeholder="Votre identifiant" id="input-login" 
                            class="form-control" type="email" 
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" 
                            name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                        </div> 

                        <!-- {{-- le password  --}} -->
                        <label id="label-password" class="form-control-label" for="password">Mot de passe </label>
                        <div id="password">
                            <!--{{-- input du password --}} -->
                            <input type="password" class="form-control" name="password" id="input-password" value="{{ old('password') }}"
                                   placeholder="Votre mot de passe">
                        </div>
                        
                        <br />
                        <input id="input-submit" type="submit" value="Se connecter">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
