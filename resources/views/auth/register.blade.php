@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('css/style.inscription.css') }}">
@endsection

@section('content')
<div class="conteneur-crea-hist">
        <div class="carnetimg">
            <div class="center-crea-hist">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="text-center" style="margin-top: 2rem">
                        <h3><i class="far fa-edit"></i>Inscription</h3>
                    </div>
                    <div class="form-group row">
                        <!-- {{-- le nom  --}} -->
                        <label id="label-nom" class="form-control-label" for="nom">Nom </label>
                        <div id="nom">
                            <!--{{-- input du nom --}} -->
                            <input type="text" class="form-control" name="name" id="input-nom" value="{{ old('name') }}" required autofocus
                                   placeholder="Votre nom">
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div> 

                        <!-- {{-- le mail  --}} -->
                        <label id="label-mail" class="form-control-label" for="email">Adresse e-mail </label>
                        <div id="mail">
                            <!--{{-- input du mail --}} -->
                            <input type="email" class="form-control" name="email" id="input-mail" value="{{ old('email') }}" required
                                   placeholder="Votre adresse e-mail">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <!-- {{-- le password  --}} -->
                        <label id="label-password" class="form-control-label" for="password">Mot de passe </label>
                        <div id="password">
                            <!--{{-- input du password --}} -->
                            <input type="password" class="form-control" name="password" id="input-password" name="password" required
                                   placeholder="Votre mot de passe">
                        </div>

                        <!-- {{-- la confirmation du mot de passe--}} -->
                        <label id="label-confirmation" class="form-control-label" for="password_confirmation">Confirmation du mot de passe </label>
                        <div id="confirmation">
                            <!--{{-- input de la confirmation --}} -->
                            <input type="password" class="form-control" name="password_confirmation" required id="input-confirmation" value="{{ old('confirmation') }}"
                                   placeholder="Confirmez votre mot de passe">
                        </div>
                        
                        <br />
                        <input id="input-submit" type="submit" value="S'inscrire">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
