@extends('layouts.app')

@section('content')

<div class="histoires">
            <p>Le Fantastique vous passionne ? </p><p>Écrire des histoires vous intéresse ?</p>
            <p>Plongez au cœur d’aventures soigneusement rédigées par la communauté et amusez-vous à créer les vôtres !</p>
            </p>
        <ul>
            <li><a href="{{route('creer_histoire')}}">Ajouter une histoire</a></li>
            <li><a href="{{route('creer_chapitre')}}">Ajouter un chapitre</a></li>
            <li><a href="{{route('choix_histoire')}}">Lier un chapitre</a></li>
        </ul>

        </div>

    <div id="grid-main">
        <div id="genres">
            <h3 id="titre-select">Genre </h3>
            <select class="custom-select" name="genre" id="genre">
                <option selected>Tout</option>
                <option>Héroique</option>
                <option>Médiévale</option>
                <option>Moderne</option>
                <option>Mythique</option>
            </select>
        </div>

        <p id="separation"><strong>|</strong></p>

        <div id="auteurs">
            <h3 id="titre-select">Auteur </h3>
            <select class="custom-select" name="auteur" id="auteur">
                <option selected>Tous</option>
                <option>Gilles</option>
                <option>Fred</option>
                <option>Nicolas</option>
                <option>Momo</option>
            </select>
        </div>

        @foreach($histoires as $histoire)
        @php
            $chapitreP=$histoire->premierChapitre();
            @endphp
        @if($lecture->where('histoire_id',$histoire->id)->where('user_id',$id)->first()==false)
            @php
            $chapitre=$histoire->premierChapitre();
            @endphp
        @else
            @php
            $chapitre=$lecture->where('histoire_id',$histoire->id)->where('user_id',$id)->first()->chapitre_id;
            @endphp
        @endif
        <div class="main-fille">
            <div class="rectangle">
                <h3>{{$histoire->titre}}</h3>
                <p>{{$histoire->utilisateur->name}} - {{$histoire->genre->label}}</p>
                <div class="image-histoire" style="background-image:url({{$histoire->photo}})" ></div>
                <p>{!!$histoire->pitch!!}</p>



            </div>
            <div class="parti">
                @php
                $like=0;
                $dislike=0;
                @endphp

                @foreach($avis as $Avis)
                @if($Avis->histoire_id==$histoire->id and $Avis->positif==1)
                @php($like++)
                @elseif($Avis->histoire_id==$histoire->id and $Avis->positif==0)
                @php($dislike++)
                @endif
                @endforeach
                <a href="{{ route('show_histoire' ,['id'=>$chapitre]) }}">C'est parti !</a>
                <a href="{{ route('show_histoire' ,['id'=>$chapitreP]) }}">Redémarrer !</a>
                <div class="avis"><a href="{{route('like' ,['id'=>$histoire->id])}}"><img class="pouce" src="images/poucebon.png" alt="poucebon"/> : {{ $like }}</a>
                    <a href="{{route('dislike' ,['id'=>$histoire->id])}}"><img class="pouce" src="images/poucebad.png" alt="poucebad"/> : {{$dislike}}</a></div>
                @if($co and $histoire->active==0)
                    <a href="{{ route('activer' ,['id'=>$histoire->id]) }}">Activer</a>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
    <!-- Scripts -->
    <script src="http://127.0.0.1:8000/js/jquery.js"></script>
@endsection


    