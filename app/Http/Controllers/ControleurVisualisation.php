<?php

namespace App\Http\Controllers;

use App\Avis;
use App\Lecture;
use App\Chapitre;
use App\Histoire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ControleurVisualisation extends Controller
{
    public function index() {
        $idUser=Auth::id();
        $lecture=Lecture::orderBy('num_sequence', 'desc')->get();
        return view("index",["co"=>false,"id"=>$idUser,"lecture"=>$lecture,"histoires"=>Histoire::where('active', 1)->get(),"avis"=>Avis::All()]);
    }

    public function affichageParGenre($id){
        $idUser=Auth::id();
        $lecture=Lecture::orderBy('num_sequence', 'desc')->get();
        return view("index",["co"=>false,"id"=>$idUser,"lecture"=>$lecture,"histoires"=>Histoire::where('user_id',$id)->where('active',1)->get(),"avis"=>Avis::All()]);
    } 

    public function affichageParAuteur($id){
        $idUser=Auth::id();
        $lecture=Lecture::orderBy('num_sequence', 'desc')->get();
        return view("index",["co"=>false,"id"=>$idUser,"lecture"=>$lecture,"histoires"=>Histoire::where('user_id',$id)->where('active',1)->get(),"avis"=>Avis::All()]);
    }

    public function affichageMesHistoires(){
        $lecture=Lecture::orderBy('num_sequence', 'desc')->get();
        $id=Auth::id();
        $co=Auth::check();
        return view("index",["co"=>$co,"id"=>$id,"lecture"=>$lecture,"histoires"=>Histoire::where('user_id',$id)->get(),"avis"=>Avis::All()]);
    }

    public function activer($id){
        DB::table('histoire')->where('id',$id)->update(['active'=>1]);
        return (redirect(route('mesHistoire')));
    }

    public function avisPos($idH){
        if (Auth::check()){
            $idU = Auth::id();
            DB::table('avis')
                ->where('user_id', $idU)
                ->where('histoire_id', $idH)->delete();
            DB::table('avis')->insert(['positif'=>1, 'histoire_id'=>$idH, 'user_id'=>$idU]);
            return redirect(route('home'));
        }
        return redirect(route('login'));

    }

    public function avisNeg($idH){
        if(Auth::check()){
            $idU = Auth::id();
            DB::table('avis')
                ->where('user_id', $idU)
                ->where('histoire_id', $idH)->delete();
            DB::table('avis')->insert(['positif'=>0, 'histoire_id'=>$idH, 'user_id'=>$idU]);
            return redirect(route('home'));
        }
        return redirect(route('login'));
    }
}
