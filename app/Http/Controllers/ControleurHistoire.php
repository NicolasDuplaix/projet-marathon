<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Suite;
use App\Lecture;
use App\Chapitre;
use App\Histoire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ControleurHistoire extends Controller
{
    public function show($id){
        $chapitre=Chapitre::find($id);
        if($chapitre->premier == 1 && Auth::check()) {
            DB::table('lecture')->where('user_id',Auth::id())->where('histoire_id',$chapitre->histoire->id)->delete();
        DB::table('lecture')->insert(
            [
                'user_id'=>Auth::id(),
                'chapitre_id'=>$id,
                'histoire_id'=>$chapitre->histoire_id,
                'num_sequence'=>1,
            ]
            );
        }
        $chapitres=Chapitre::All();
        $lecture=Lecture::where('user_id',Auth::id())->where('histoire_id',$chapitre->histoire->id)->get();
        return view('histoire.show',["chapitres"=>$chapitres,"chapitre"=>$chapitre,"lectures"=>$lecture]);
    }

    public function store($id){
        $suite=Suite::find($id);
        if (Auth::check()){
            $chapitreId=$suite->chapitre_source_id;
            $chapitre=Chapitre::find($chapitreId);
            $lectures=Lecture::where('histoire_id',$chapitre->histoire_id)
            ->where('user_id',Auth::id())
            ->where('chapitre_id',$chapitreId)
            ->first();
            DB::delete("Delete FROM lecture WHERE user_id=? AND histoire_id=? and num_sequence > ?",[Auth::id(), $chapitre->histoire_id, $lectures->num_sequence]); 

            DB::table('lecture')->insert(
                [
                    'user_id'=>Auth::id(),
                    'chapitre_id'=>$suite->chapitre_destination_id,
                    'histoire_id'=>$chapitre->histoire_id,
                    'num_sequence'=>($lectures->num_sequence)+1
                ]
            );
        }
        return redirect(route('show_histoire' ,['id'=>$suite->chapitre_destination_id]));
    }


}