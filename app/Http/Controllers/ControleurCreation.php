<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Chapitre;
use App\Histoire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ControleurCreation extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    //---------------------------------------------------------
    public function creerHistoire() {
        if (Auth::check()){
            return view('creer_histoire', ["genres"=>Genre::All()]);
        }
        else{
            return redirect('/login');
        }
    }

    public function enregistrerHistoire(Request $request) {
        $this->validate(
            $request,
            [
                "titre"=>'required',
                "pich"=>'required',
                "photo"=>'nullable',
                "genre"=>'required'
            ]
        );

        $photo = null;

        if (request()->hasFile('photo')) {
            $photo = $request['photo']->store('/public/images/'.Auth::id());
            $photo= str_replace("public","storage",$photo);
        }

        $input = $request->only(["titre","pich","photo","genre"]);
        DB::table('histoire')->insert(
            [
                "titre"=>$input['titre'],
                "user_id"=>Auth::id(),
                "pitch"=>$input['pich'],
                "photo"=>$photo,
                "genre_id"=>$input['genre'],
                'active'=>0
            ]
        );
        return redirect(route('creer_chapitre'));
    }


    //---------------------------------------------------------

    public function creerChapitre() {
        return view('creer_chapitre', ["histoires"=>Histoire::All()]);
    }

    public function enregistrerChapitre(Request $request) {
        $this->validate(
            $request,
            [
                "titre"=>'required',
                "titrecourt"=>'required',
                "texte"=>'required',
                "question"=>'nullable',
                "photo"=>'nullable|image|mimes:jpg,jpeg,png,gif',
                "histoire"=>'required',
                "premier"=>'required'
            ]
        );
        $photo = null;
        if (request()->hasFile('photo')) {
            $photo = $request['photo']->store('/public/images/'.Auth::id());
            $photo= str_replace("public","storage",$photo);
        }

        $input = $request->only(["titre","titrecourt","texte","histoire","question","premier"]);
        DB::table('chapitre')->insert(
            [
                "titre"=>$input['titre'],
                "titrecourt"=>$input['titrecourt'],
                "photo"=>$photo,
                "texte"=>$input['texte'],
                "histoire_id"=>$input['histoire'],
                "premier"=>$input['premier'],
                "question"=>$input['question']
            ]
        );
        return redirect(route('creer_chapitre'));
    }


    //---------------------------------------------------------

    public function choixHistoire(){
        return view('choix_histoire',["histoires"=>Histoire::where('user_id',Auth::id())->get()]);
    }

    public function enregistrerChoix(Request $request) {
        $this->validate(
            $request,
            [
                "histoire"=>'required'
            ]
        );
        $input = $request->only(["histoire"]);

        return (redirect(route('lier_chapitre', ['id'=>$input['histoire']])));
    }

    public function lierChapitre($id) {
        $chapitre=Chapitre::where('histoire_id',$id)->get();
        return view('lier_chapitre',["chapitres"=>$chapitre]);
    }

    public function enregistrerLiaison(Request $request) {
        $this->validate(
            $request,
            [
                "reponse"=>'required',
                "dest"=>'required',
                "source"=>'required'
            ]
            );
        $input = $request->only(["reponse","dest","source"]);
        DB::table('suite')->insert(
            [
                "reponse"=>$input["reponse"],
                "chapitre_source_id"=>$input["source"],
                "chapitre_destination_id"=>$input["dest"]
            ]
        );
        return redirect(route('choix_histoire'));
    }
}
