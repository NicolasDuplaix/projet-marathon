<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/histoire/creer', 'ControleurCreation@creerHistoire')->name('creer_histoire');
Route::post('/histoire/enregistrer', 'ControleurCreation@enregistrerHistoire')->name('enregistrer_histoire');


Route::get('/', 'ControleurVisualisation@index')->name('home');
Route::get('/auteur/{id}', 'ControleurVisualisation@affichageParAuteur')->name('triAuteur');
Route::get('/meshistoires', 'ControleurVisualisation@affichageMesHistoires')->name('mesHistoire');

Route::get('/genre/{id}','ControlleurVisualisation@affichageParGenre')->name('affichage_par_genre');


Route::get('/chapitre/creer', 'ControleurCreation@creerChapitre')->name('creer_chapitre');
Route::post('/chapitre/enregistrer', 'ControleurCreation@enregistrerChapitre')->name('enregistrer_chapitre');

Route::get('/histoire/lier', 'ControleurCreation@choixHistoire')->name('choix_histoire');
Route::post('/histoire/lier/enregistrer', 'ControleurCreation@enregistrerChoix')->name('enregistrer_choix');

Route::get('/chapitre/lier/{id}', 'ControleurCreation@lierChapitre')->name('lier_chapitre');
Route::post('/chapitre/lier/enregistrer', 'ControleurCreation@enregistrerLiaison')->name('enregistrer_liaison');

Route::get('/chapitre/{id}', 'ControleurHistoire@show')->name('show_histoire');

Route::get('/suite/{id}', 'ControleurHistoire@store')->name('store_avancement');

Route::get('/histoire/{id}/like', 'ControleurVisualisation@avisPos')->name('like');
Route::get('/histoire/{id}/dislike', 'ControleurVisualisation@avisNeg')->name('dislike');

Route::get('/histoire/{id}/activ', 'ControleurVisualisation@activer')->name('activer');